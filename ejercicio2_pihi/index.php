<?php 
// esta etiqueta siempre debe de ir para activar la sesion
session_start();
?>
<html>
	<head>
		<title> Inicio de Sesión </title>
		<style>
			button {
				padding: 10px;
				font-size: 1.5rem;
				font-family: 'Lato';
				font-weight: 100;
				background: yellowgreen;
				color: white;
				border: none;
			}
			body {
				margin: 50px auto;
				text-align: center;
				width: 800px;
			}
			input {
				border: 2px solid #ccc;
				font-size: 1.5rem;
				font-weight: 100;
				font-family: 'Lato';
				padding: 10px;
			}
			form {
					margin: 25px auto;
					padding: 20px;
					border: 5px solid #ccc;
					width: 500px;
					background: #eee;
				}

				div.form-element {
					margin: 20px 0;
				}
		</style>
	</head>
	<body>
		<center>
		<p>Inicio de Sesión</p>
		<form action="info.php?accion=get&texto=textoenget" method="POST">
			<div class="form-element">
			<label>Número de Cuenta</label>
			<input type="text" name="numCta" pattern="[a-zA-Z0-9]+" required />
			</div>
			<div class="form-element">
			<label>Contraseña</label>
			<input type="password" name="password" required />
			</div>
			<button type="submit" name="login" value="login">Iniciar</button>
			</form>
		<?php 
		// las sesiones se pueden ocupan en cualqueir lado despues del session_start();
		//$_SESSION['numCta'] = 'numCta';
		//$_SESSION['nombre'] = 'nombre';
		//$_SESSION['pasatiempos'] = ['', '', ''];
		$_SESSION['usuario']=[
			1 => [
				'numCta' => '1',
				'nombre' => 'Admin',
				'pApellido' => 'General',
				'sApellido' => '',
				'password' => 'adminpass123.',
				'sexo' => 'Otro',
				'date' => '25/01/1990'
			],
			2 => [
				'numCta' => '2',
				'nombre' => 'Hugo',
				'pApellido' => 'Cuéllar',
				'sApellido' => 'Martínez',
				'password' => 'minpass1234.',
				'sexo' => 'Mujer',
				'date' => '28/02/1994'
			]
		];
		//Funcionalidad inicio de sesión
		if (isset($_POST['login'])) {
		
			$username = $_POST['numCta'];
			$password = $_POST['password'];
		}
		//Restringiendo las páginas
		if(!isset($_SESSION['numCta'])){
			header('Location: index.php');
			exit;
		} else {
			// Show users the page!
		}
		?>
		<p></p>
		<a href='./register.php'>Registrarse</a>&nbsp;&nbsp;&nbsp;&nbsp;


		
	
	</body>
</html>
<html>
<head>
    <title>Formularios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"
          href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
</head>
    <body>
	
	<div class="container">
		<div class="columns">
			<center>
				Llena los siguientes campos para registrarte
			<!--
				En la etiqueta form son importantes los siguientes atributos method y action
				method para especificar el método en que se va a enviar el formulario "get" o "post" cambien el tipo de metodo para ver cambios
				action para especificar el archivo que va a procesar la información
			-->
			<form action="info.php?accion=get&texto=textoenget" method="POST">
				<!-- form numCta control -->
				<label class="form-label" for="input-text">Número de Cuenta</label>
				<input name="texto" class="form-input " type="text" id="input-numCta" placeholder="numCta">
				<!-- form nombre control -->
				<label class="form-label" for="input-text">Nombre(s)</label>
				<input name="nombre" class="form-input " type="text" id="input-nombre" placeholder="nombre">
				<!-- form pApellido control -->
				<label class="form-label" for="input-text">Primer apellido</label>
				<input name="pApellido" class="form-input " type="text" id="input-pApellido" placeholder="pApellido">
				<!-- form sApellido control -->
				<label class="form-label" for="input-text">Segundo apellido</label>
				<input name="sApellido" class="form-input " type="text" id="input-sApellido" placeholder="sApellido">
				<!-- form radio control -->
				<label class="form-label">Sexo</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="Hombre" checked>
					<i class="form-icon"></i> Hombre
				</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="Mujer">
					<i class="form-icon"></i> Mujer
				</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="Otro">
					<i class="form-icon"></i> Otro
				</label>
				<label class="form-label" for="input-date">Fecha de Nacimiento</label>
				<input name="date" class="form-input " type="date" id="input-date"
					   placeholder="date">
				

				<!-- form textarea control -->
				<!--<label class="form-label" for="input-textarea">Mensaje</label> 
				<textarea name="textarea" class="form-input" id="input-textarea"
						  placeholder="Textarea" rows="3"></textarea> -->

				<!-- form select control -->
				<label class="form-label" for="input-select">¿Que programa usas?</label>
				<select id="input-select" class="form-select" name="red_social">
					<option value=''>--Elige uno--</option>
					<option value='Skype'>Skype</option>
					<option value='Telegram'>Telegram</option>
					<option value='Twitter'>Twitter</option>
					<option value='Whatsapp'>Whatsapp</option>
				</select>

				

				<!-- form checkbox control -->
				<label class="form-label">Pasatiempos</label>
				<label class="form-checkbox">
					<input type="checkbox" name="pasatiempo" value="Videojuegos">
					<i class="form-icon"></i> Videojuegos </input>
				</label>
				<label class="form-checkbox">
					<input type="checkbox" name="pasatiempo" value="Deportes">
					<i class="form-icon"></i> Deportes </input>
				</label>
				<label class="form-checkbox">
					<input type="checkbox" name="pasatiempo" value="Lectura">
					<i class="form-icon"></i> Lectura </input>
				</label>

				<!-- HTML 5 controls -->
				<!-- form date control -->
				

				<!-- form email control -->
				<label class="form-label" for="input-email">Email</label>
				<input name="email" class="form-input " type="email" id="input-email"
					   placeholder="email">

				<!-- form password control -->
				<label class="form-label" for="input-password">Contraseña</label>
				<input name="password" class="form-input" type="password" id="input-password"
					   placeholder="Contraseña">

				<!-- Botones -->
				<input type='submit' class="btn" value="Enviar"/>
				<input type='reset' class="btn btn-primary" value="limpiar"/>
			</form>
		</div>
	</div>
    </body>
</html>